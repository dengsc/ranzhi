FROM ubuntu:trusty

ADD   http://dl.cnezsoft.com/ranzhi/4.4/ranzhi.4.4.stable.zbox_64.tar.gz  /tmp

COPY  ./boot.sh   /usr/local/boot.sh
RUN   chmod +x    /usr/local/boot.sh

ENTRYPOINT  ["/usr/local/boot.sh"]
